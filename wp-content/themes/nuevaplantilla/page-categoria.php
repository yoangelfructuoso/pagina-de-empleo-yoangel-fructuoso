<?php 
/**
 * 
 * 
 * Template Name: Plantilla de categorias
 * 
 */
?>

<!DOCTYPE html>
<html lang="en">


<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>EmpleosJobs </title>

   <?php wp_head() ?>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top is-fixed is-visible" id="mainNav">
    <div class="container">
      <a class="navbar-brand" style="color: #42A8D5 "  href="index.php">EmpleosJobs</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" style="color: #42A8D5 ;" href="">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" style="color: #42A8D5 ;" href="<?php echo site_url('?page_id=9') ?>">Categorias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" style="color: #42A8D5 ;"href="<?php echo site_url('?page_id=11') ?>">Contactos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  style="color: #42A8D5 ;" href="?page_id=13">Sobre nosotros</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('https://besthqwallpapers.com/Uploads/6-5-2020/132244/thumb2-cloud-technologies-blue-cloud-technology-background-digital-technology-background-network-technology-blue-line-cloud.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto  ">
          <div class="site-heading" style="color: #D7E5E6">
            <h1>EmpleosJobs</h1>
            <span class="subheading" style="color: white ">Tecnologia 

            </span>
          </div>
        </div>
      </div>
    </div>
  </header>

   <!-- Categories Widget -->
 <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="card mb-4" style="margin-left: 0%;">
          <h5 class="card-header" style=" color: #42A8D5; text-transform: uppercase;">Que area te interesa</h5>
          <div class="card-body1">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="?cat=2" style="text-transform: uppercase; ">Tecnologia (IT) </a>
                  </li>
                  <li>
                    <a href="?cat=5" style="text-transform: uppercase; ">Administacion</a>
                  </li>
                  <li>
                    <a href="?cat=7" style="text-transform: uppercase; ">Derecho </a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="?cat=4" style="text-transform: uppercase;">Contabilidad</a>
                  </li>
                  <li>
                    <a href="?cat=1" style="text-transform: uppercase;">Magisterio</a>
                  </li>
                  <li>
                    <a href="?cat=6" style="text-transform: uppercase;">Turismo</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
    
</body>

</html>
<?php  

while (have_posts()){
  the_post(get_the_category('tecnologia'));
?>



  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
         <h3 style="color: #42A8D5;">  <?php  the_title(); ?>  </h3> 

        <?php   the_content(); ?>
          
          
        </div>
      </div>
    </div>
    
  </article>





<?php 
}
wp_reset_postdata();
get_footer();
?>


