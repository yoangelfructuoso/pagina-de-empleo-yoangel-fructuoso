<?php  get_header(); 
while (have_posts()){
  the_post();
?>



  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
         <h3 style="color: #42A8D5;">  <?php  the_title(); ?>  </h3> 

        <?php   the_content(); ?>
          
          
        </div>
      </div>
    </div>
  </article>





<?php 
}

wp_reset_postdata();
get_footer();
?>