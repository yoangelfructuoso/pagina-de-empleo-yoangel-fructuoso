<?php    

get_header();

// Yoangel Fructuoso
?>

<!-- Page Header -->
<header class="masthead" style="background-image: url('https://images8.alphacoders.com/368/thumb-1920-368904.jpg')">
    <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto  ">
            <div class="site-heading" style="color: #D7E5E6">
              <h1>EmpleosJobs</h1>
              <span class="subheading" style="color: white ">Aqui encontrras nuevas oportunidades

            </span>
          </div>
        </div>
      </div>
    </div>
</header>
      
     
 <!-- Categories Widget -->
 <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="card mb-4" style="margin-left: 0%;">
          <h5 class="card-header" style=" color: #42A8D5; text-transform: uppercase;">Que area te interesa</h5>
          <div class="card-body1">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="?cat=2" style="text-transform: uppercase; ">Tecnologia (IT) </a>
                  </li>
                  <li>
                    <a href="?cat=5" style="text-transform: uppercase; ">Administacion</a>
                  </li>
                  <li>
                    <a href="?cat=7" style="text-transform: uppercase; ">Derecho </a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="?cat=4" style="text-transform: uppercase;">Contabilidad</a>
                  </li>
                  <li>
                    <a href="?cat=1" style="text-transform: uppercase;">Magisterio</a>
                  </li>
                  <li>
                    <a href="?cat=6" style="text-transform: uppercase;">Turismo</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <?php while(have_posts()) {
         the_post();
        categorias_post_type();
         
        
    ?>

     <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
          <a href="<?php the_permalink();?>">
            <h2 class="post-title">
              <?php  the_title();?>
            </h2>
            <h3 class="post-subtitle">
              <?php wp_trim_words(get_the_content(),12); ?>
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">Start Bootstrap</a>
            on September 24, 2019</p>
        </div>
        <hr>
      </div>
    </div>
  </div>
  <?php  }wp_reset_postdata();?>

  <!-- Pager -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="clearfix">
          <a class="btn btn-primary float-right"   href="#">Mas vacantes &rarr;</a>
        </div>
      </div>
    </div>
  </div>
  

  
<?php
get_footer(); 

?>