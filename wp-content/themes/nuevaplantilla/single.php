<?php  get_header(); 

while (have_posts()){
  the_post();
  
?>
 <!-- Page Header -->
<header class="masthead" style="background-image: url('https://images8.alphacoders.com/368/thumb-1920-368904.jpg')">
    <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto  ">
            <div class="site-heading" style="color: #D7E5E6">
              <h1>EmpleosJobs</h1>
              <span class="subheading" style="color: white ">Aqui encontrras nuevas oportunidades

            </span>
          </div>
        </div>
      </div>
    </div>
</header>


  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
         <h3 style="color: #42A8D5;">  <?php  the_title(); ?>  </h3> 

        <?php   the_content(); ?>
          
          
        </div>
      </div>
    </div>
    
  </article>





<?php 
}
wp_reset_postdata();
get_footer();
?>